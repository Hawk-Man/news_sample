// $(document).ready(function(){

//   var masterData;//取得したjsonデータ保存用
//   var tempData ;//処理用のjson
//   var commentHtml;//jsonから生成したHTML
//   var commentDom;//挿入するdom用
//   var commentFlag = false;//コメントフラグ
//   var interval = 5000;//コメント表示時間

//   // Jsonデータの取得
//   $('.js-updateJsonBtn').on('click', function() {
//     getJson();
//   });

//   // スプレッドシートデータの取得
//   $('.js-updateSheetBtn').on('click', function() {
//     getSheet();
//   });

//   // スプレッドシートみんなのコメントを取得
//   $('.js-updateSheetEveryoneBtn').on('click', function() {
//     getSheetEveryone();
//   });

//   // コメントスタート
//   $('.js-stratBtn').on('click', function() {
//     commentFlag = true;
//     if(masterData == undefined) {
//       alert('データを取得してください！');
//       return;
//     }
//     tempData = JSON.parse(JSON.stringify(masterData));
//     domDisply();
//   });

//   // コメントストップ
//   $('.js-stopBtn').on('click', function() {
//     commentFlag = false;
//   });

//   // jsonの取得
//   function getJson(){
//       $.ajaxSetup({async: false});//同期通信(json取ってくるまで待つ)
//       $.getJSON("text.json", function(data){
//           masterData = JSON.parse(JSON.stringify(data));
//       });
//       $.ajaxSetup({async: true});
//   }
//   // スプレッドシートからデータ取得
//   function getSheet(){
//       $.ajax({
//         type: 'GET',
//         url: 'https://spreadsheets.google.com/feeds/cells/1lsF8TAH8GmRL30Lf6fob3SSaflcZAlnkmpO3g9XbMok/od6/public/values?alt=json',
//         dataType: 'jsonp',
//         cache: false,
//         success: function(data){ // 通信が成功した時
//           var sheetsEntry = data.feed.entry; // 実データ部分を取得
//           masterData = categorizeData(sheetsEntry); // データを整形して配列で返す
//         },
//         error: function(){ // 通信が失敗した時
//           console.log('error');
//         }
//       });
//   }

//   // スプレッドシートからみんなのコメント取得
//   function getSheetEveryone(){
//       $.ajax({
//         type: 'GET',
//         url: 'https://spreadsheets.google.com/feeds/cells/1lsF8TAH8GmRL30Lf6fob3SSaflcZAlnkmpO3g9XbMok/ok0cimp/public/values?alt=json',
//         dataType: 'jsonp',
//         cache: false,
//         success: function(data){ // 通信が成功した時
//           var sheetsEntry = data.feed.entry; // 実データ部分を取得
//           var getData;
//           getData = categorizeData(sheetsEntry); // データを整形して配列で返す
//           // commentTypeを追加
//           for (var i = 0; i < getData.length; i++) {
//             getData[i].commentType = '2';
//           }
//           masterData = getData;
//         },
//         error: function(){ // 通信が失敗した時
//           console.log('error');
//         }
//       });
//   }

//   // データを整形して配列で返す
//   function categorizeData(sheetsEntry){
//     var keyWord = [];
//     var categorized = [];
//     for(var i = 0; i < sheetsEntry.length; i++) {
//       var dataCol = sheetsEntry[i].gs$cell.col;
//       var dataRow = sheetsEntry[i].gs$cell.row;
//       var dataTxt = sheetsEntry[i].gs$cell.$t;
//       if(dataRow == 1) {
//         keyWord.push(dataTxt);
//       }else if(dataCol == 1){
//         var item = {};
//         var itemKey = keyWord[0];
//         item[itemKey] = dataTxt;
//         categorized.push(item);
//       }else {
//         var targetObject = categorized[dataRow-2];
//         var itemKey = keyWord[dataCol-1];
//         targetObject[itemKey] = dataTxt;
//       }
//     }
//     return categorized;
//   }

//   // dom表示
//   function domDisply() {
//     // フラグ判定
//     if(!(commentFlag)) return;
//     // パラメータが無くなっていれば追加
//     if(Object.keys(tempData).length==0) {
//       $('.js-text').html(commentHtml);
//       tempData = JSON.parse(JSON.stringify(masterData));
//     };
//     param = tempData[0];
//     //jsonを基にHTML生成
//     switch(param.commentType) {
//       // 速報コメント用
//       case '1':
//         commentHtml ='<li>'
//                    +'<div>速報！！！</div>'
//                    +'<div>ビンゴ者：' + param.bingoName + '</div>'
//                    +'<div>当選者：' + param.winnerName + '</div>'
//                    +'<div>商品：' + param.ProductName + '</div>'
//                    +'<div>' + param.comment + '</div>'
//                    +'</li>';
//         break;
//       // 通常コメント用
//       case '2':
//         commentHtml ='<li>'
//                    +'<div>' + param.comment + '</div>'
//                    +'<div>投稿者：' + param.contributorName + '</div>'
//                    +'</li>';
//         break;
//       default:
//         commentHtml ='<li>'
//                     +'<div>楽しんでね！（データミス、、、、）</div>'
//                     +'</li>';
//         break;
//     }
//     // アニメーションの追加
//     commentDom = $(commentHtml).css('left', 2000).animate({'left':0}, 2000).delay(10000).remove();
//     // DOMに追加
//     $('.js-text').append(commentDom);
//     // 処理済みのパラメータ削除
//     tempData.shift();
//     // 次の回の実行予約
//     setTimeout(function(){
//         domDisply();
//     }, interval);
//   }




// });
// function submitJson() {
//   var $textarea = $('.js-textarea')
//   var text = $textarea.val();
//   $textarea.val('');
//   console.log(text);
// }