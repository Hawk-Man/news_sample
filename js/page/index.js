$(document).ready(function(){
  var json;
  getSheet();
  // スプレッドシートからみんなのコメント取得
  function getSheet(){
      $.ajax({
        type: 'GET',
        url: 'https://spreadsheets.google.com/feeds/cells/1AEKmT4ueU5PWrQW4soKIv_IG4iQfT7fg4IdWAIvVBs0/od6/public/values?alt=json',
        dataType: 'jsonp',
        cache: false,
        success: function(data){ // 通信が成功した時
          var sheetsEntry = data.feed.entry; // 実データ部分を取得
          var getData = categorizeData(sheetsEntry); // データを整形して配列で返す
          console.log(getData);// 整形データ内容を表示
          json = getData;
          dom(json);
        },
        error: function(){ // 通信が失敗した時
          console.log('error');
        }
      });
  }
  // データを整形して配列で返す
  function categorizeData(sheetsEntry){
    var keyWord = [];
    var categorized = [];
    for(var i = 0; i < sheetsEntry.length; i++) {
      var dataCol = sheetsEntry[i].gs$cell.col;
      var dataRow = sheetsEntry[i].gs$cell.row;
      var dataTxt = sheetsEntry[i].gs$cell.$t;
      if(dataRow == 1) {
        keyWord.push(dataTxt);
      }else if(dataCol == 1){
        var item = {};
        var itemKey = keyWord[0];
        item[itemKey] = dataTxt;
        categorized.push(item);
      }else {
        var targetObject = categorized[dataRow-2];
        var itemKey = keyWord[dataCol-1];
        targetObject[itemKey] = dataTxt;
      }
    }
    return categorized;
  }

  function dom(data) {
    var dom = '';
    for (var i = 0; i < data.length; i++) {
      dom += '<li><p>' + data[i].comment + '</p><p>投稿者：' + data[i].name + '</p></li>';
    }
    console.log(dom)
    $('.js-text2').html(dom.replace(/\r?\n/g, '<br>'));
  }

});