$(document).ready(function(){

  var masterData;//取得したjsonデータ保存用
  var tempData ;//処理用のjson
  var commentHtml;//jsonから生成したHTML
  var commentDom;//挿入するdom用
  var commentFlag = false;//コメントフラグ
  var interval = 4000;//コメント表示時間
  var commentType = 1;//コメントの種類
  var commentDisplayTime = 20000;//コメント表示時間
  var maxComment = 20;//コメント表示数



  // プログラム変更
  $('.js-programItem').find('li').on('click', function() {
    $(this).siblings().removeClass('current');
    $(this).addClass('current');
  });

  getSheetEveryone();
$(function(){
  setTimeout(function(){
    tempData = JSON.parse(JSON.stringify(masterData));
    commentDisplay();
  },1000);
});


  // スプレッドシートからみんなのコメント取得
  function getSheetEveryone(){
      $.ajax({
        type: 'GET',
        url: 'https://spreadsheets.google.com/feeds/cells/1lsF8TAH8GmRL30Lf6fob3SSaflcZAlnkmpO3g9XbMok/ok0cimp/public/values?alt=json',
        dataType: 'jsonp',
        cache: false,
        success: function(data){ // 通信が成功した時
          var sheetsEntry = data.feed.entry; // 実データ部分を取得
          var getData;
          getData = categorizeData(sheetsEntry); // データを整形して配列で返す
          // commentTypeを追加
          for (var i = 0; i < getData.length; i++) {
            getData[i].commentType = '2';
          }
          masterData = getData;
        },
        error: function(){ // 通信が失敗した時
          console.log('error');
        }
      });
  }

  // データを整形して配列で返す
  function categorizeData(sheetsEntry){
    var keyWord = [];
    var categorized = [];
    for(var i = 0; i < sheetsEntry.length; i++) {
      var dataCol = sheetsEntry[i].gs$cell.col;
      var dataRow = sheetsEntry[i].gs$cell.row;
      var dataTxt = sheetsEntry[i].gs$cell.$t;
      if(dataRow == 1) {
        keyWord.push(dataTxt);
      }else if(dataCol == 1){
        var item = {};
        var itemKey = keyWord[0];
        item[itemKey] = dataTxt;
        categorized.push(item);
      }else {
        var targetObject = categorized[dataRow-2];
        var itemKey = keyWord[dataCol-1];
        targetObject[itemKey] = dataTxt;
      }
    }
    return categorized;
  }

  // dom表示
  function commentDisplay() {
    // パラメータが無くなっていれば追加
    if(Object.keys(tempData).length == 0 ) {
      getSheetEveryone();
      tempData = JSON.parse(JSON.stringify(masterData));
    }else if(Object.keys(tempData).length == Object.keys(masterData).length -maxComment) {
      getSheetEveryone();
      tempData = JSON.parse(JSON.stringify(masterData));
    }

    // domの生成へ
    param = tempData[Object.keys(tempData).length-1];
    //jsonを基にHTML生成
    if( 7 < commentType) commentType = 1;
    commentHtml = '<li class="commontType' + commentType + '">' + param.comment + '</li>';
    commentType ++;
    // アニメーションの追加
    commentDom = $(commentHtml).fadeIn(400).delay(commentDisplayTime).fadeOut(400, function() {
      $(this).remove();
    });;
    // DOMに追加
    $('.js-commentItem').append(commentDom);
    // 処理済みのパラメータ削除
    tempData.pop();
    // 次の回の実行予約
    setTimeout(function(){
        commentDisplay();
    }, interval);
  }




});